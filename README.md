# LV keyboard for Sailfish #

### Setup ###

* Copy the `lv.qml` file to `/usr/share/maliit/plugins/com/jolla/layouts/`.
* Add an entry to `layouts_western.conf` in that same folder:

```
[lv.qml]
name=Latviešu
languageCode=LV
```

* Restart `maliit-server` using `killall maliit-server`.
* Navigate to Settings->System->Text input. The new entry should be under Keyboards.

The steps are taken from `https://together.jolla.com/question/21510/howto-adapt-sailfish-virtual-keyboard-vkb-layout/`.
To execute them it will be necessary to enable developer mode on the Sailfish
device.

